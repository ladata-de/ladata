# -*- coding: utf-8 -*-

"""Main module."""
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from freckworks.project import FreckworksProjectBase
from freckworks.tasklets.freckles import FreckletTasklet

# from freckworks.tasklets.lxd import LxdContainers, CreateLxdImage
from ladata.defaults import LADATA_FRECKLETS_PATH


class LadataProject(FreckworksProjectBase):
    def __init__(self, config, **kwargs):

        self._db_engine = None
        self._db_session = None
        super(LadataProject, self).__init__(config=config, **kwargs)

    @property
    def db_ip(self):

        # return self.get_property("machines.servers.server_db.ip")
        return "192.168.178.209"

    @property
    def db_engine(self):

        # db_user = self.get_property("services.databases.warehouse.username")
        # db_pass = self.get_property("services.databases.warehouse.password")
        db_user = "ladata"
        db_pass = "ladata"
        if self._db_engine is None:
            self._db_engine = create_engine(
                "postgresql://{}:{}@{}/ladata".format(db_user, db_pass, self.db_ip)
            )

        return self._db_engine

    @property
    def db_session(self):

        if self._db_session is None:
            self._db_session = sessionmaker(bind=self.db_engine)

        return self._db_session

    @contextmanager
    def session_scope(self):
        """Provide a transactional scope around a series of operations."""

        session = self.db_session()
        try:
            yield session
            session.commit()
        except (Exception):
            session.rollback()
            raise
        finally:
            session.close()

    def get_extra_repos(self):

        return [LADATA_FRECKLETS_PATH]

    def get_defaults(self):

        # return {
        #     "debug": {
        #         "input": ["inp_one", "inp_two"],
        #         # "input": "one",
        #         "input2": ["inp_three", "inp_four"]
        #     }
        # }

        return {
            "lxd_base_image": {
                "source_image": "images:debian/10",
                "image_name": "ladata-base-image",
                # "image_server": "https://images.linuxcontainers.org",
                "image_server": None,
                "freckles_extra_repos": self.get_extra_repos(),
                "frecklet_name": "ladata-base-image-built",
            },
            "machines": {"server_names": ["server-0"]},
        }

    def create_project_tasks(self):

        context_config = ["community", "dev"]
        extra_repos = [LADATA_FRECKLETS_PATH]

        # create_baseimage = CreateLxdImage(
        #     alias="create_base_image",
        #     input={
        #         "frecklet_name": "{{:: lxd_base_image.frecklet_name ::}}",
        #         "image_name": "{{:: lxd_base_image.image_name ::}}",
        #         "source_image": "{{:: lxd_base_image.source_image ::}}",
        #         "extra_repos": "{{:: lxd_base_image.freckles_extra_repos ::}}",
        #     },
        #     run_strategy="always",
        # )
        #
        # servers = LxdContainers(
        #     alias="create_servers",
        #     target="machines.server_details",
        #     input={
        #         "machine_names": "{{:: machines.server_names ::}}",
        #         "image_name": "{{:: lxd_base_image.image_name ::}}",
        #         "image_server": "{{:: lxd_base_image.image_server ::}}",
        #     },
        #     context_config=context_config,
        #     extra_repos=extra_repos,
        # )

        def db_postprocess(input, result):
            import pp

            pp(input)
            return (input["host_details"]["ip"],)

        install_db = FreckletTasklet(
            frecklet_name="ladata-db",
            alias="install_db",
            context_config=context_config,
            extra_repos=extra_repos,
            run_config="{{:: machines.servers.server_db.init_user ::}}@{{:: machines.servers.server_db.ip ::}}",
            input={
                "host_details": "{{:: machines.servers.server_db ::}}",
                "db_user": "{{:: services.databases.warehouse.db_user ::}}",
                "db_user_password": "{{:: services.databases.warehouse.db_user_password ::}}",
                "db_name": "{{:: services.databases.warehouse.db_name ::}}",
            },
            postprocess_callback=db_postprocess,
            target="services.databases.warehouse.ip",
        )

        init_system = FreckletTasklet(
            frecklet_name="ladata-init-system",
            alias="init_system",
            context_config=context_config,
            extra_repos=extra_repos,
            run_config="{{:: machines.servers.server_ladata.init_user ::}}@{{:: machines.servers.server_ladata.ip ::}}",
            input={
                "admin_user": "{{:: machines.admin_user ::}}",
                "install_python": True,
            },
        )

        install_metabase = FreckletTasklet(
            frecklet_name="metabase-service",
            alias="install_metabase",
            context_config=context_config,
            extra_repos=extra_repos,
            run_config="{{:: machines.admin_user ::}}@{{:: machines.servers.server_ladata.ip ::}}",
            input={
                "metabase_host": "{{:: machines.servers.server_ladata.ip ::}}",
                "metabase_db_host": "{{:: machines.servers.server_db.ip ::}}",
                "metabase_db_port": 5432,
                "metabase_db_name": "metabase",
                "metabase_db_user": "metabase",
                "metabase_db_password": "metabase",
                "metabase_db_type": "postgres",
            },
            target="services.bi.metabase",
        )

        # delete_servers = FreckletTasklet(
        #     frecklet_name="lxd-container-absent",
        #     multiplier=["name"],
        #     input={"name": "{{:: machines.server_names ::}}"},
        #     run_strategy="always",
        #     alias="delete_servers",
        #     invalidate_state=True,
        # )

        def jtl_singer_postprocess(input, result):
            import pp

            pp(input)
            return input["path"] + "/execute-pipe.sh"

        singer_pipeline_jtl = FreckletTasklet(
            frecklet_name="singer-pipeline-jtl",
            alias="singer_pipeline_jtl",
            context_config=context_config,
            extra_repos=extra_repos,
            run_config="{{:: machines.admin_user ::}}@{{:: machines.servers.server_ladata.ip ::}}",
            input={
                "path": "/home/freckworks/pipelines/jtl",
                "target_host": "{{:: services.databases.warehouse.ip[0] ::}}",
                "target_user": "{{:: services.databases.warehouse.db_user ::}}",
                "target_password": "{{:: services.databases.warehouse.db_user_password ::}}",
                "target_db_name": "{{:: services.databases.warehouse.db_name ::}}",
                "jtl_host": "{{:: services.databases.jtl.host ::}}",
                "jtl_user": "{{:: services.databases.jtl.user ::}}",
                "jtl_password": "{{:: services.databases.jtl.password ::}}",
                "jtl_database_name": "{{:: services.databases.jtl.db_name ::}}",
            },
            target="services.singer.jtl.path_script",
            provides="{{:: __result__ ::}}",
            postprocess_callback=jtl_singer_postprocess,
        )

        install_dbt = FreckletTasklet(
            frecklet_name="dbt-installed",
            alias="install_dbt",
            context_config=context_config,
            extra_repos=extra_repos,
            run_config="{{:: machines.admin_user ::}}@{{:: machines.servers.server_ladata.ip ::}}",
            input={"path": "/home/freckworks/ladata-transform"},
            target="services.dbt",
        )

        install_ladata = FreckletTasklet(
            frecklet_name="ladata-installed",
            alias="install_ladata",
            context_config=context_config,
            extra_repos=extra_repos,
            run_config="{{:: machines.admin_user ::}}@{{:: machines.servers.server_ladata.ip ::}}",
            input={"config": self.config.root_config},
            target="services.ladata",
        )

        return [
            # servers,
            # create_baseimage,
            install_db,
            # delete_servers,
            init_system,
            singer_pipeline_jtl,
            install_metabase,
            install_dbt,
            install_ladata,
        ]

        # return [debug_one, debug_two, debug_three]
