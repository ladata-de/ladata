"""warehouse_id

Revision ID: 15f61103a87a
Revises: 45475896ef07
Create Date: 2019-10-17 14:36:13.021207

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '15f61103a87a'
down_revision = '45475896ef07'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
