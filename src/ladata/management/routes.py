# -*- coding: utf-8 -*-

from flask import Blueprint, current_app
from flask_restful import Api, Resource
from frutils.exceptions import FrklException

management = Blueprint(
    "management", __name__, static_folder="static", template_folder="templates"
)
management_api = Api(management)

# @management.route("/")
# def index():
#     print("XXXX")
#     return url_for("management.static", filename="index.html")

# import pp
# task_manager = current_app.project.task_manager
# running_tasks = task_manager.running_tasks
# return render_template('index.html', state=current_app.project.state, tasks=current_app.project.project_tasks, running_tasks=running_tasks)


class State(Resource):
    def get(self):

        state = current_app.project.state
        return state


class Task(Resource):
    def get(self, task_alias):

        status = current_app.run_manager.get_run_status(task_alias)
        return status

    def post(self, task_alias):

        print("task_alias" + task_alias)

        try:
            current_app.run_manager.start_run(
                task_alias,
                return_result_dicts=True,
                run_id=task_alias,
                tags=["provisioning"],
            )
        except (FrklException) as fe:
            print(fe)
            pass
        except (Exception) as e:
            print(e)
            pass
        return {"run_id": task_alias}


@management.route("/test")
def test():
    print("TESTING")

    # pp(current_app.__dict__)
    # socketio = current_app.extensions["socketio"]

    return "success", 200


class Tasks(Resource):
    def get(self):

        return current_app.run_manager.get_all_runs_status()


management_api.add_resource(Task, "/run/<string:task_alias>")

management_api.add_resource(Tasks, "/runs")
management_api.add_resource(State, "/state")
