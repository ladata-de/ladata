# -*- coding: utf-8 -*-
import os

import click
from ruamel.yaml import YAML

from freckworks.cli.utils import create_cli
from ladata.app.core import create_app
from ladata.defaults import LADATA_CONFIG_PATH

yaml = YAML()

cli = create_cli(
    project_type="ladata",
    alias="ladata",
    path=LADATA_CONFIG_PATH,
    create_project_path=True,
    config_file_name="ladata.yml",
)


@cli.command()
@click.option(
    "--debug", "-d", help="start webserver in debug/development mode", is_flag=True
)
def web(debug):
    app, socketio = create_app()
    if not debug:

        socketio.run(app, port=8000, host="0.0.0.0")
        # gunicorn_app = GUnicornFlaskApplication(app)
        # gunicorn_app.run(
        #     worker_class="gunicorn.workers.sync.SyncWorker",
        # )
    else:
        socketio.run(app, debug=True, port=8000, host="0.0.0.0")


@cli.group()
def db():

    pass


@db.command()
def upgrade():

    migrations_dir = os.path.join(os.path.dirname(__file__))
    versions_dir = os.path.join(migrations_dir, "alembic", "versions")

    if not os.path.exists(versions_dir):
        os.makedirs(versions_dir)

    import alembic.config

    alembicArgs = ["--raiseerr", "upgrade", "head"]
    os.chdir(migrations_dir)
    alembic.config.main(argv=alembicArgs)


@db.command()
@click.argument("message", nargs=1)
def schema_update(message):

    migrations_dir = os.path.join(os.path.dirname(__file__))
    versions_dir = os.path.join(migrations_dir, "alembic", "versions")

    if not os.path.exists(versions_dir):
        os.makedirs(versions_dir)

    import alembic.config

    alembicArgs = ["--raiseerr", "revision", "--autogenerate", "-m", message]
    os.chdir(migrations_dir)
    alembic.config.main(argv=alembicArgs)
