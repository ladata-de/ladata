# -*- coding: utf-8 -*-
import copy
import logging
import threading
import time
import uuid
from collections import OrderedDict
from datetime import datetime

from fasteners import ReaderWriterLock, write_locked
from frutils.exceptions import FrklException
from six import string_types

from freckworks.utils import flatten_results_list

log = logging.getLogger("freckworks")


class FreckworksProjectCallback(object):
    def __init__(self, callback_alias):

        self._alias = callback_alias

    @property
    def alias(self):
        return self._alias

    def run_started(self, run_details):
        pass

    def run_finished(self, run_details):
        pass

    def state_changed(self, new_state):
        pass


class RunManager(object):
    def __init__(self, project, project_callbacks=None, socket_io=None):

        self._lock = ReaderWriterLock()
        self._project = project
        self._running_tasks = OrderedDict()
        self._archived_tasks = OrderedDict()

        self._cleanup_thread = None
        self._socket_io = socket_io

        if project_callbacks is None:
            project_callbacks = OrderedDict()
        self._project_callbacks = project_callbacks

    @property
    def running_tasks(self):
        return self._running_tasks

    @property
    def archived_tasks(self):
        return self._archived_tasks

    def get_task_status(self, task_id):

        pass

    def add_project_callback(self, callback):

        self._project_callbacks[callback.alias] = callback

    def _send_callback_event(self, event_type, value=None):

        if event_type == "state_changed":
            new_state = self._project.state
            for cb in self._project_callbacks.values():
                cb.state_changed(new_state=new_state)
        elif event_type == "run_started":
            for cb in self._project_callbacks.values():
                cb.run_started(run_details=value)
        elif event_type == "run_finished":
            for cb in self._project_callbacks.values():
                cb.run_finished(run_details=value)

    @write_locked
    def start_run(
        self, task_aliases, return_result_dicts=False, run_id=None, tags=None
    ):

        if run_id is None:
            run_id = uuid.uuid4().hex

        if tags is None:
            tags = []

        if isinstance(tags, string_types):
            tags = [tags]

        # if run_id in self._running_tasks.keys():
        #     print("ALREADY RUNNING: {}".format(run_id))
        #     return run_id

        for tag in tags:
            for r_id, r in self._running_tasks.items():
                if tag in r["tags"]:
                    raise FrklException(
                        msg="Can't start run with id: {}".format(run_id),
                        reason="Run with the tag '{}' is currently running: {}".format(
                            tag, r_id
                        ),
                    )

        def run_flow():

            # executor = SimpleExecutor(self)

            try:
                self._send_callback_event(
                    "run_started", {"run_id": run_id, "status": "running"}
                )
                results = self._project.execute(
                    task_aliases=task_aliases,
                    return_result_dicts=return_result_dicts,
                    run_id=run_id,
                )
                if not return_result_dicts:
                    results = flatten_results_list(results)
                success = True
                error = None
            except (Exception) as e:
                success = False
                error = e
                results = []

            task_details = self._running_tasks.pop(run_id)
            start_time = task_details["start_time"]
            end_time = datetime.timestamp(datetime.utcnow())
            r = {
                "results": results,
                "start_time": start_time,
                "end_time": end_time,
                "tags": tags,
                "success": success,
                "error": str(error),
                "run_id": run_id,
                "status": "finished",
            }
            self._archived_tasks[run_id] = r
            self._send_callback_event("run_finished", r)
            self._send_callback_event("state_changed")

            return results

        now = datetime.timestamp(datetime.utcnow())
        # bg_thread = self._socket_io.start_background_task(run_flow)
        bg_thread = threading.Thread(target=run_flow)
        bg_thread.start()
        print("THREAD STARTED")
        self._running_tasks[run_id] = {
            "task_thread": bg_thread,
            "start_time": now,
            "tags": tags,
        }
        # self._running_tasks[run_id]["task_thread"].start()

        return run_id

    def get_run_status(self, run_id):

        if run_id in self._running_tasks.keys():
            return {"status": "running"}
        elif run_id in self._archived_tasks.keys():
            result = copy.deepcopy(self._archived_tasks[run_id])
            result["status"] = "finished"
            return result
        else:
            return {"status": "absent"}

    def get_all_runs_status(self):

        result = {}
        for run_id in self._project.project_tasks.keys():
            result[run_id] = self.get_run_status(run_id)
        return result

    def start_cleanup_thread(self):
        """Cleanup old tasks.
        """

        if self._cleanup_thread is not None:
            return self._cleanup_thread

        def cleanup():
            while True:

                # five_min_ago = datetime.timestamp(datetime.utcnow()) - 5 * 60
                print("CLEANUP")

                # remove = []
                # for task_id, task in running_tasks.items():
                #     if 'completion_timestamp' in task and task['completion_timestamp'] > five_min_ago:
                #         remove.append(task_id)
                #
                # for r in remove:
                #     running_tasks.pop(r, None)

                time.sleep(60)

        self._cleanup_thread = threading.Thread(target=cleanup)
        self._cleanup_thread.start()
