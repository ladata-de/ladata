# -*- coding: utf-8 -*-
import os

LADATA_PROJECT_CONFIG_NAME = "ladata.yml"

LADATA_FRECKLETS_PATH = os.path.join(os.path.dirname(__file__), "external", "frecklets")
LADATA_CONFIG_PATH = os.path.join(os.path.expanduser("~"), ".config", "ladata")
