# -*- coding: utf-8 -*-
from sqlalchemy import Column, String, Integer, TIMESTAMP

from ladata.db.models import Base


class ItemScan(Base):
    @classmethod
    def from_dict(cls, data):

        # tid = data["tid"]
        # sid = data["sid"]
        # udid = data["udid"]
        # userid = data["userid"]
        # questions = data.get("questions", None)
        # answers = data["answers"]
        # capture_type = data.get("capture_type", None)
        # time_zone = data.get("time_zone", None)
        # gps_location = data.get("gps_location", None)

        obj = ItemScan()

        obj.userid = data["userid"]
        obj.ean = data["tid"]
        obj.sid = data["sid"]
        obj.deviceid = data["deviceid"]
        obj.udid = data["udid"]
        obj.capture_type = data["capture_type"]
        # obj.timestamp= data["device_timestamp"]
        obj.timestamp = data["scanned_at_utc"]
        obj.barcode_format = data["barcode_format"]
        obj.amount = data.get("answers[1686298]", 1)
        obj.warehouse_id = data["answers[1759908]"]

        return obj

    __tablename__ = "item_scans"
    __table_args__ = {"schema": "stocktake"}
    amount = Column(Integer, default=1)
    sid = Column(Integer)
    id = Column(Integer, primary_key=True)
    ean = Column(String)
    userid = Column(Integer)
    deviceid = Column(Integer)
    udid = Column(String)
    capture_type = Column(String)
    timestamp = Column(TIMESTAMP(timezone=True))
    barcode_format = Column(String)
    warehouse_id = Column(Integer)

    def __repr__(self):
        return "id: {}, EAN: {}".format(self.id, self.ean)
