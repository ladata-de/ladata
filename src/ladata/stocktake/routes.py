# -*- coding: utf-8 -*-
from flask import Blueprint, request, Response, current_app

from ladata.stocktake.models import ItemScan

stocktake = Blueprint("stocktake", __name__)


@stocktake.route("/")
def index():

    print("hello!!!!")

    # item = ScannedItem(name="lego")
    #
    # db.session.add(item)
    # db.session.commit()

    return "status: ok", 200


@stocktake.route("/scan_item", methods=["POST"])
def register_scan():

    data = request.form

    import pp

    pp(data)
    item = ItemScan.from_dict(data)

    project = current_app.project

    with project.session_scope() as session:
        session.add(item)
        session.commit()

    xml = """<?xml version="1.0" encoding="UTF-8"?>
<xml>
    <message>
        <status>1</status>
        <text>Thank you for scanning with codeREADr</text>
    </message>
</xml>"""

    return Response(xml, mimetype="text/xml")
