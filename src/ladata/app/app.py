# -*- coding: utf-8 -*-

from ladata.app.core import create_app

app, socketio = create_app()
