# -*- coding: utf-8 -*-
import logging

from flask import Blueprint

main = Blueprint("main", __name__, static_folder="static")
logger = logging.getLogger()


# @main.route("/")
# def view_base():
#     print(app.ladata_project.project_tasks.keys())
#     return jsonify(
#         {"status": "success", "tasks": list(app.ladata_project.project_tasks.keys())}
#     )
