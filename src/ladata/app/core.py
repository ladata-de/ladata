# -*- coding: utf-8 -*-
import logging
import os

import socketio
from flask import Flask
from flask import send_from_directory
from flask_cors import CORS
from flask_socketio import SocketIO, emit

from freckworks.project import get_project
from ladata.defaults import LADATA_CONFIG_PATH
from ladata.run_management import RunManager, FreckworksProjectCallback

logger = logging.getLogger()

CONFIG_KEYS = []

running_tasks = {}


class SocketIOProjectCallback(FreckworksProjectCallback):
    def __init__(self):

        super(SocketIOProjectCallback, self).__init__(callback_alias="socketio")
        self._sio = socketio.Client()
        self._connected = False

    def connect(self):
        if self._connected:
            return

        self._sio.connect("http://localhost:8000")
        self._connected = True

    def emit(self, event_name, details):
        self.connect()
        self._sio.emit(event_name, details)

    def state_changed(self, new_state):
        self.emit("state_changed", new_state)

    def run_started(self, run_details):
        print("RUN_STARTED")
        self.emit("run_started", run_details)

    def run_finished(self, run_details):
        print("RUN_FINISHED")
        self.emit("run_finished", run_details)


def create_app(debug=False):
    return entrypoint(debug=debug)


def entrypoint(debug=False):

    app = Flask(__name__, static_url_path="", static_folder="static")
    # app.config.from_mapping({"EXPLAIN_TEMPLATE_LOADING": True, "TEMPLATES_AUTO_RELOAD": True})
    CORS(app)
    socketio = SocketIO(app, cors_allowed_origins="*")

    app.debug = debug

    configure_app(app)
    configure_logging(debug=debug)

    from ladata.app.main import main
    from ladata.stocktake.routes import stocktake
    from ladata.management.routes import management

    # register blueprints
    app.register_blueprint(main, url_prefix="")
    app.register_blueprint(management, url_prefix="/manage")
    app.register_blueprint(stocktake, url_prefix="/stocktake")

    project = get_project(
        alias="ladata",
        path=LADATA_CONFIG_PATH,
        project_type="ladata",
        create_project_path=True,
        config_file_name="ladata.yml",
    )
    app.project = project
    socketio_callback = SocketIOProjectCallback()
    app.run_manager = RunManager(
        project=project,
        project_callbacks={"socket_ip": socketio_callback},
        socket_io=socketio,
    )

    # @app.before_first_request
    # def before_first_request():
    #     """Start a background thread that cleans up old tasks."""
    #     app.project.task_manager.start_cleanup_thread()

    # api = Api(app)
    # Handler for a message recieved over 'connect' channel
    # @socketio.on('connect')
    # def test_connect():
    #     print("XXXXXXXXXXXX")
    #     print("connected")
    #
    # @socketio.on('disconnect')
    # def test_disconnect():
    #     print("YYYYYYYYYYYYYYY")
    #     print("disconnected")

    @socketio.on("state_changed")
    def incoming_state(message):
        emit("state_changed", message, broadcast=True)

    @socketio.on("run_started")
    def incoming_run_started(message):
        emit("run_started", message, broadcast=True)

    @socketio.on("run_finished")
    def incoming_run_finished(message):
        emit("run_finished", message, broadcast=True)

    @app.route("/")
    def index():
        return send_from_directory("static", "index.html")

    return app, socketio


def configure_app(app):

    logger.info("configuring flask app")

    for key in CONFIG_KEYS:
        value = os.environ.get(key)
        if value is None:
            raise Exception("Missing config value: {}".format(key))
        logger.debug("Adding config key: {}".format(key))
        app.config[key] = os.environ.get(key)

    app.ladata_project = get_project(alias="ladata", path=None, project_type="ladata")


def configure_logging(debug=False):

    root = logging.getLogger()
    h = logging.StreamHandler()
    fmt = logging.Formatter(
        fmt="%(asctime)s %(levelname)s (%(name)s) %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S",
    )
    h.setFormatter(fmt)

    root.addHandler(h)

    if debug:
        root.setLevel(logging.DEBUG)
    else:
        root.setLevel(logging.INFO)
