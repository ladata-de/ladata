# -*- coding: utf-8 -*-
from sqlalchemy import Column, String, Integer

from ladata.db.models import Base


class Article(Base):

    __tablename__ = "articles"
    id = Column(Integer, primary_key=True)
    ean = Column(String)
    amount = Column(Integer, default=0)
