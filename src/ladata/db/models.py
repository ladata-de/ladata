# -*- coding: utf-8 -*-
from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class TestItem(Base):

    __tablename__ = "test_items"
    id = Column(Integer, primary_key=True)
    ean = Column(String)

    def __repr__(self):
        return "id: {}, EAN: {}".format(self.id, self.ean)
