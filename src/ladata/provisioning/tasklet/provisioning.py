# -*- coding: utf-8 -*-
from freckworks.tasklets.freckles import FrecklesTasklet


class LadataAnalyzeInstalled(FrecklesTasklet):
    def __init__(
        self, alias=None, target=None, input=None, context_config=None, extra_repos=None
    ):
        super(LadataAnalyzeInstalled, self).__init__(
            alias=alias,
            target=target,
            input=input,
            context_config=context_config,
            extra_repos=extra_repos,
        )

    def execute(self, pyckles, input):

        meltano_installed = self.create_pycklet(
            pyckles, "ladata-analyze-installed", user="service", group="service"
        )

        self.run_pycklets(
            pyckles,
            meltano_installed,
            run_config=self.create_run_config(target_string=input.get("target_string")),
        ),

    def requires(self):

        return ["target_string"]

    def provides(self):

        return []
