[![PyPI status](https://img.shields.io/pypi/status/ladata.svg)](https://pypi.python.org/pypi/ladata/)
[![PyPI version](https://img.shields.io/pypi/v/ladata.svg)](https://pypi.python.org/pypi/ladata/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/ladata.svg)](https://pypi.python.org/pypi/ladata/)
[![Pipeline status](https://gitlab.com/frkl/ladata/badges/develop/pipeline.svg)](https://gitlab.com/frkl/ladata/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# ladata

*Daten management fuer den Einzelhandel*


## Description

Documentation still to be done.

# Usage

Download the ``ladata`` binary from: https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/ladata, make it executable and add it to your path. For example:

```console
wget https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/ladata
chmod +x ladata
mkdir -p ~/.local/bin
mv ladata ~/.local/bin

# adjust PATH (optional)
echo 'export PATH=$PATH:~/.local/bin' >> "~/.profile"
source ~/.profile
```

# Development

## Using freckles

``` console
frecklecute gl:ladata-de::develop::/ladata/ladata-dev-project.frecklet --project-base <absolute_project_base_folder>
```
## Manual

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'ladata' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 ladata
    git clone https://gitlab.com/frkl/ladata
    cd <ladata_dir>
    pyenv local ladata
    pip install -e '.[develop,testing,docs]'
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
