#!/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "${THIS_DIR}"

frecklecute -t freckworks@192.168.178.20 -c ladata-dev update-ladata-service
