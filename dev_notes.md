
# local development

# update local source code

``
<ladata_src>/../update-git-repos.sh
``

## update local source code (incl. install new depencies if necessary)

``
<ladata_src>/../update-data-dev.sh
``

# dev service

## ladata dev service details

- ladata dev service host: 192.168.178.20
- ladata dev service user: freckworks


## update dev service

This updates ladata, ladata-transform & pjtl source & restarts ladata service on the ladata service host. Execute locally (on dev machine):

```
./update-ladata.service.sh
```

## singer pipeline details

- path: ``/home/freckworks/pipelines/jtl``
- state_file: ``/home/freckworks/pipelines/jtl/state.json``
- update command (execute on ladata host): ``/home/freckworks/pipelines/jtl/execute.sh``

## dbt pipeline details

- path: /home/freckworks/ladata-transform
- dbt command:

```
cd /home/freckworks/ladata-transform
dbt run
```
